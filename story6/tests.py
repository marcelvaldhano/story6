from django.test import TestCase, Client
from django.urls import resolve
from django.utils import timezone
from .views import *
from .models import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
import chromedriver_binary

# Create your tests here.
class Testing(TestCase):
    def test_tampilan_is_exist(self):
        response=Client().get('/test/')
        self.assertEqual(response.status_code,200)
    
    def test_view_using_view_template(self):
        response=Client().get('/test/')
        self.assertTemplateUsed(response,'test.html')

    def test_tampilan_using_tampilan_func(self):
        found=resolve('/test/')
        self.assertEqual(found.func,test)

    def test_can_save_a_POST_request(self):
        response = Client().post('/test/', data={'message': 'test', 'date':'2019-10-28T16:00'})
        counting = Data.objects.count()
        self.assertEqual(response.status_code, 302)
        self.assertEqual(counting, 1)
        self.assertEqual(response['location'], '/test/')
        more = Client().get('/test/')
        html_response = more.content.decode('utf8')
        self.assertIn('test', html_response)

    def test_models_can_create_status(self):
        s=Data.objects.create(message="Kuliah hanya jadi beban hidup",date=timezone.now())
        counting=Data.objects.count()
        self.assertEqual(counting,1)

    
class NewStatus(unittest.TestCase):
    def setUp(self):
        chrome_options=Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome(chrome_options=chrome_options)

    def tearDown(self):
        self.browser.quit()
        
    def test_can_post_status_and_find_it(self):
        self.browser.get('http://localhost:8000/test')
        message = self.browser.find_element_by_id('data')
        send = self.browser.find_element_by_id('send')
        message.send_keys('tiket mahal cuk')
        send.send_keys(Keys.RETURN)
        results = self.browser.find_element_by_class_name('stats').text
        self.assertIn('tiket mahal cuk', results)
    
    def test_300_max_length(self):
        self.browser.get('http://localhost:8000/test')
        message = self.browser.find_element_by_id('data')
        send = self.browser.find_element_by_id('send')
        message.send_keys('tesapakahbisa300katatesapakahbisa300katatesapakahbisa300katatesapakahbisa300katatesapakahbisa300katatesapakahbisa300katatesapakahbisa300katatesapakahbisa300katatesapakahbisa300katatesapakahbisa300katatesapakahbisa300katatesapakahbisa300katatesapakahbisa300katatesapakahbisa300katatesapakahbisa300kata')
        send.send_keys(Keys.RETURN)
        results = self.browser.find_element_by_class_name('stats').text
        self.maxDiff = None
        self.assertEqual(len(results), 300)




    





    
