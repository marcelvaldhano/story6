from django import forms
from django.forms import widgets

class DataForm(forms.Form):
	message = forms.CharField(widget=forms.TextInput(attrs={"id":"data", "maxlength":"300", "placeholder": "Tuliskan komentar kamu disini"}))
