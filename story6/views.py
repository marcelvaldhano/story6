from django.shortcuts import render,redirect
from .models import Data
from .forms import DataForm


# Create your views here.
def test(request):
    if request.method == 'POST':
        form = DataForm(request.POST)
        if form.is_valid():
            m = form.cleaned_data['message']
            s = Data(message=m)
            s.save()
            return redirect('test')

    else:
        form = DataForm()

    data = Data.objects.order_by("-date")
    return render(request,'test.html', {'form': form, "data": data})    

